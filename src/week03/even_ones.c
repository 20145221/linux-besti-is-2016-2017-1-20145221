#include<stdio.h>

int even_ones(unsigned x)
{
	x ^= x>>16;
	x ^= x>>8;
	x ^= x>>4;
	x ^= x>>2;
	x ^= x>>1;
	return !(x&1);
}

int main()
{
	int n;
	printf("inpur a num:");
	scanf("%d", &n);
	printf("%d\n", even_ones(n));
	return 0;
}
