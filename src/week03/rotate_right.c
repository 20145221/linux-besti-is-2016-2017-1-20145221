#include<stdio.h>

unsigned rotate_right(unsigned x, int n)
{
	int w = sizeof(unsigned) << 3;
	return (x>>n) | (x<<(w-n));
}

int main()
{
	int x, n;
	printf("input x and n:  ");
	scanf("%x%d", &x, &n);
	printf("%x\n", rotate_right(x, n));
	return 0;
}
