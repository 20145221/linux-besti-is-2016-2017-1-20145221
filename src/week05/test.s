g:
	pushl	%ebp		;将%ebp入栈，为帧指针
	movl	%esp, %ebp	
	movl	8(%ebp), %eax	;get x
	addl	$21, %eax	;x+21
	popl	%ebp		;%ebp出栈
	ret

f:
	pushl	%ebp		;将%ebp入栈，为帧指针
	movl	%esp, %ebp	
	pushl	8(%ebp)
	call	g		;调用函数g
	addl	$4, %esp
	leave
	ret

main:
	pushl	%ebp
	movl	%esp, %ebp
	pushl	$0
	call	f		;调用f函数
	addl	$4, %esp
	addl	$20140000, %eax	
	leave
	ret
