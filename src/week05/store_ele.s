	.file	"store_ele.c"
	.comm	A,2772,32
	.text
	.globl	store_ele
	.type	store_ele, @function
store_ele:
.LFB0:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	movl	12(%ebp), %edx
	movl	8(%ebp), %ecx
	movl	%edx, %eax
	sall	$3, %eax
	addl	%edx, %eax
	movl	%ecx, %edx
	sall	$6, %edx
	subl	%ecx, %edx
	addl	%eax, %edx
	movl	16(%ebp), %eax
	addl	%edx, %eax
	movl	A(,%eax,4), %edx
	movl	20(%ebp), %eax
	movl	%edx, (%eax)
	movl	$2772, %eax
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE0:
	.size	store_ele, .-store_ele
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.2) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
