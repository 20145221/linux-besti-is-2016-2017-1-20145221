#ifndef  ARGV_H
#define  ARGV_H

/* 
 *功能：把命令行字符串转化为以NULL结尾的参数数组.
 *参数：s: IN, 命令行字符串
 *      delimiters: IN,分割符
 *      argvp:OUT, 指向参数数组的指针
 *返回值：
 *      成功：返回标记的个数
 *      错误：返回-1, 并设置errno
 * */
int makeargv(const char *s, const char *delimiters, char ***argvp);

void freemakeargv(char **argv);
#endif   /* ----- #ifndef ARGV_H_INC  ----- */
